<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/users/{name}/{id}', function($name, $id) {
//     return "Boa tarde: " . $name . " seu número é: " . $id;
// });

//Configuração de Rotas
//Aonde o usuário será redirecionadao para a area determinada

//Chamando as view pelo Cotroller
//a VIEW será chamada pelo Controller

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

//Fazer todas as rotas do controller
Route::resource('posts','PostsController');
Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
