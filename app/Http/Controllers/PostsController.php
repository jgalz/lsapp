<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Post;

//For DB SIMPLE
//use DB;

class PostsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
         $this->middleware('auth', ['except' => ['index', 'show']] );
     }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Post::all();
        //return Post::where('title', 'Post Two')->get();
        //$posts = DB::select('SELECT * FROM posts');
        //$posts =  Post::orderBy('title', 'desc')->take(1)->get();
        //$posts =  Post::orderBy('title', 'desc')->get();


        $posts =  Post::orderBy('created_at', 'desc')->paginate(5);
        return view('posts/index')->with('posts',$posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Required Fields
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'cover_image' => 'image|nullable|max:1999' 
        ]);

        //Hamdle File Upload

        if($request->hasFile('cover_image') ) {
            //Get a file name with a extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();

            //Get Just File Name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            //Get Just Ext
            $extension = $request->file('cover_image')->guessClientExtension();

            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            //Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        //Create Post
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');   
        $post->user_id = auth()->user()->id;
        $post->cover_image = $fileNameToStore;
        $post->save();
    
        //Redirect with a message
        return redirect('/posts')->with('success', 'Post Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Show Posts
        $post = Post::find($id);
        return view('posts/show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Edit Post
        $post = Post::find($id);
        
        //Check user
        if(auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        return view('posts/edit')->with('post', $post);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        //Required Fields
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

         //Hamdle File Upload

        if($request->hasFile('cover_image') ) {
            //Get a file name with a extension
            $filenameWithExt = $request->file('cover_image')->getClientOriginalName();
            //Get Just File Name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get Just Ext
            $extension = $request->file('cover_image')->guessClientExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);
        } 
   
        //Update Post

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');   
        if($request->hasFile('cover_image')) {
            $post->cover_image = $fileNameToStore;
        }
        $post->save();
    
        //Redirect with a message
        return redirect('/posts')->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Delete Post
        $post = Post::find($id);
        
        //Check user
        if(auth()->user()->id !== $post->user_id) {
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        if($post->cover_image != 'noimage.jpg') {
            //Delete Image
            Storage::delete('public/cover_images/'.$post->cover_image);
        }


        $post->delete();

        //Redirect with a message
        return redirect('/posts')->with('success', 'Post Deleted');

    }
}
