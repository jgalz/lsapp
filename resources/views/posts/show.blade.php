@extends('layouts/app')

@section('content')


<div class="jumbotron">
    <div class="container">
        <h1>{{ $post->title }}</h1>
        <a href="/posts" class="btn btn-dark mt-3 mb-2">Go Back</a>
    </div>
</div>

<div class="container">
        <img style="width:50%" src="/storage/cover_images/{{ $post->cover_image }}" alt="">
    <p> {!! $post->body !!} </p>   
    <small>Written on {{ $post->created_at }} by {{ $post->user->name }} </small>
</div>

@guest
    <!-- Visitor Logged -->
@else
    <!-- User Logged -->
    <!-- Only Owner of your own post cand edit or delete -->
    @if(Auth::user()->id == $post->user_id)
        
        <div class="container">
            <br>
            <a class="btn btn-secondary mb-5" href="/posts/{{ $post->id }}/edit">Edit</a>

            {{ Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right' ]) }}
                {{ Form::hidden('_method', 'DELETE') }}
                {{ Form::submit('Delete', ['class' => 'btn btn-danger mb-5']) }}
            {{ Form::close() }}
        </div>

    @endif

@endguest

        
@endsection