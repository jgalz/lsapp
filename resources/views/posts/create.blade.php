@extends('layouts/app')

@section('content')


<div class="jumbotron">
    <div class="container">
        <h1>Create Post</h1>
        <a href="/posts" class="btn btn-dark mt-3 mb-2">Go Back</a>
    </div>
</div>

<div class="container">
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data'] ) !!}
    
        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title' ]) }}
        </div>
        <div class="form-group">
            {{ Form::label('title', 'Body') }}
            {{ Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text' ]) }}
        </div>
        <div class="form-group">
            {{ Form::file('cover_image') }}
        </div>
        {{ Form::submit('Create', ['class' => 'btn btn-success mb-5']) }}

    {!! Form::close() !!}
</div>
        
@endsection