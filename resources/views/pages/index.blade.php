@extends('layouts/app')

@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
    <h1 class="display-4">{{ $title }}</h1>
      <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
      <button class="btn btn-primary">Login</button>
      <button class="btn btn-success">Register</button>
    </div>
</div>
@endsection
