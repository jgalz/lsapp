@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <a href="/posts/create" class="btn btn-primary">Create Post</a>
                    <h3 class="mt-2">Your Blog Posts</h3>
                </div>
            </div>
            @if(count($posts) > 0)
                 <table class="table table-striped">
                    <tr>
                        <th>Title</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                    @foreach ($posts as $post)
                    <tr>
                        <th>{{ $post->title }}</th>
                        <th>
                            <a href="/posts/{{$post->id}}" class="btn btn-primary">View Post</a>
                        </th>
                        <th>
                            <a href="/posts/{{$post->id}}/edit" class="btn btn-success">Edit Post</a>
                        </th>
                        <th>
                            {{ Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST']) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                            {{ Form::close() }}
                        </th>
                    </tr>
                    @endforeach
                </table>
            @else    
                <p>You have no posts.</p>
            @endif 
        </div>
    </div>
</div>
@endsection
